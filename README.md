# Elasticsearch role

An [Ansible](https://www.ansible.com/) role for installing
[Elasticsearch](https://www.elastic.co/) on distributions based on
[RHEL](https://fr.wikipedia.org/wiki/Red_Hat_Enterprise_Linux).

## Defining roles

Create a YAML file `roles.yml`.

Example for creating a role to access one single index through Kibana:

```yaml
---
- name: role1
  desc:
    indices:
      - names: ['myindex']
        privileges: ['all']
    applications:
      - application: "kibana-.kibana*"
        privileges: ["*"]
        resources: ["*"]
```

And set the path to this file into `es_roles_file` variable.

## Defining users

Create a YAML file `users.yml`:

```yaml
---
- name: user1
  profile:
    - password: mypassword
      roles: ["role1", "role2"]
      full_name: User Name
      email: user@some.site
```

And set the path to this file into `es_users_file` variable.

## Additional tasks

**Generate a Kibana access token:**

To generate an access token for Kibana,
set the variable `es_gen_kb_token` to `true` in your playbook (default: `false`).

*Related variables*

- `es_kb_token_name`: name of the generated token (default: `genelastic`),
- `es_kb_token_file`: storage path of the generated token on the remote host
  (default: `{{ ansible_env.HOME }}/es_kb_token.txt`).

**Generate API keys:**

To generate API keys, you must provide paths to JSON files detailing the API key attributes.
These files constitute the body that will be included in POST requests made to the Elasticsearch server.

All obtained API keys will be stored as JSON files in the directory of your choice on the local machine.

*Related variables*

- `es_api_keys_req_bodies`: A list of file paths to JSON templates defining API key request attributes (default: `[]`).
  These files are processed using the template module, allowing them to include variables.
- `es_api_keys_out_dir`: The directory where all obtained API keys will be stored on the local machine.
  An API key file is named after the `name` attribute present in the API key request body.
  This directory must exist beforehand (default: `""`).

<details>
<summary><i>Example</i></summary>

In your playbook, define the following variables:

```yaml
es_api_keys_req_bodies:
  - "{{ playbook_dir }}/api_keys_req_bodies/my_req_body.json"
es_api_keys_out_dir: "{{ playbook_dir }}/api_keys/"
```

`{{ playbook_dir }}/api_keys_req_bodies/my_req_body.json` content:

```json
{
  "name": "full_access_api_key",
  "role_descriptors": {
    "full_access_role": {
      "indices": [
        {
          "names": [
            "*"
          ],
          "privileges": [
            "all"
          ]
        }
      ],
      "cluster": [
        "all"
      ]
    }
  }
}
```

Running the role will create the API key file `{{ playbook_dir }}/api_keys/full_access_api_key.json`,
where the filename is the name of the requested key `full_access_api_key`.

```json
{
  "id": "...",
  "name": "full_access_api_key",
  "api_key": "...",
  "encoded": "..."
}
```

</details>

## Role variables

**Elasticsearch config**

- `es_data_path`: path to the directory where Elasticsearch stores indexed data

**JVM options**

- `es_jvm_xms`: set the initial memory allocation pool for the JVM
  (default: let Elasticsearch automatically set its value),
- `es_jvm_xmx`: set the maximum memory allocation pool for the JVM
  (default: let Elasticsearch automatically set its value).
