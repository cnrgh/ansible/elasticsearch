all:

check:
	ansible-lint -s -x name .

docker:
	docker build -t ansible-elasticsearch .

.PHONY: all check
